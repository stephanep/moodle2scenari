<?php
error_reporting(E_ALL & ~E_NOTICE);
setlocale(LC_ALL, "fr_FR.UTF-8");
putenv('LC_ALL=fr_FR.UTF-8');

function getRandomString($length){
	$chars = 'abcdefghijklmnopqrstuvwxyz0123456789';
	$result = '';
	while(strlen($result)<$length) {
		$result .= $chars{mt_rand(0,strlen($chars)-1)};
	}
	return $result;
}

function error($text) {
	require_once("header.inc.php");
	echo($text);
	require_once("footer.inc.php");
	exit(1);
}


// function from http://stackoverflow.com/a/834355/3104853
function endswith($haystack, $needle)
{
	$length = strlen($needle);
	if ($length == 0) {
		return true;
	}
	
	return (substr($haystack, -$length) === $needle);
}


// look in a directory recursively to find a file that contains "$filename"
function findfile($dir, $filename) {
	$iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($dir), RecursiveIteratorIterator::SELF_FIRST);
	
	foreach ($iterator as $path) {
		if (!$path->isDir()) {
			$pathstr=$path->__toString();
			if (strpos($pathstr, $filename) !== false) {
				return($pathstr);
			}
		}
	}
	return false;
}


function printlogs($cmdout) {
	if (!count($cmdout) || !trim($cmdout[0]))
		return;
	echo '<pre class="logs">';
	foreach ($cmdout as $cmdoutline)
		echo htmlspecialchars($cmdoutline). "\n";
	echo "</pre>";
}

if (!empty($_FILES)) {
	$id=getRandomString(16);
	$pathroot = '/tmp/upload/'.$id.'/';
	$pathin = $pathroot . 'in/';
	$pathout = $pathroot. 'import-exercices-moodle/';
	if (!file_exists($pathout)) {
		if (!mkdir($pathout, 0700, true)) {
			error("Erreur interne : impossible de créer le répertoire de sortie du fichier : ".$pathroot);
		}
	}
	if (!file_exists($pathin)) {
		if (!mkdir($pathin, 0700, true)) {
			error("Erreur interne : impossible de créer le répertoire de stockage du fichier : ".$pathin);
		}
	}
	$filein = $pathroot . "moodle.xml";
	$fileout = $pathroot . "exercices-moodle.scar";
	$pathfinal = __DIR__ . '/upload/'.$id.'/';
	if (!file_exists($pathfinal)) {
		if (!mkdir($pathfinal, 0700, true)) {
			error("Erreur interne : impossible de créer le répertoire de diffusion : ".$pathfinal);
		}
	}
	
	if (!move_uploaded_file($_FILES['file']['tmp_name'], $filein)) {
		error("Erreur interne : le fichier envoyé n'a pu être chargé correctement : ".$filein);
	}
	
	
	chdir($pathroot);
	require_once("header.inc.php");

	echo "fichier accepté... Traitement en cours\n\n";
	exec("python3 /opt/moodle2scenari/moodle2scenari.py $filein $pathout 2>&1", $cmdout, $errcode);
	if ($errcode===0 && file_exists($pathout) && count(glob($pathout."/*.quiz", GLOB_NOSORT))>0) {
		echo "<br><b>Conversion terminée !</b><br>";
		printlogs($cmdout);
	} else {
		printlogs($cmdout);
		
		error("<br><b>Erreur lors de la conversion !</b><br>");
	}
	
	
	exec("zip -r exercices-moodle.scar import-exercices-moodle 2>&1", $cmdout, $errcode);
	if ($errcode===0 && file_exists('exercices-moodle.scar') && filesize('exercices-moodle.scar')>128) {
		rename($pathroot.'exercices-moodle.scar', $pathfinal.'exercices-moodle.scar');
		echo "<p><a href=\"./upload/$id/exercices-moodle.scar\">Téléchargez votre archive Scenari</a></p>";
	} else {
		echo '<pre>';
		printlogs($cmdout);
		error("erreur dans la production du fichier scar de contenus");
		echo '</pre>';
	}
}

require_once('header.inc.php');

?>
<h2>Objectifs</h2>
<p>
	Cet outil permet de convertir des QCM Moodle XML en un espace contenant des questions au format Scenari.
	Il fonctionne même pour les exercices qui comportent des formules LaTeX et des images (png, jpg), les tableaux simples.
	Dans cette version, ne fonctionne pas avec certains éléments moodle (vidéos, tableaux complexes, HTML imbriqué de manière particulière...), et ne supporte que les types d'exercices moodle QCM (réponse unique ou multiple), Vrai / Faux et de manière expérimentale les textes à trous
</p>
<p>
	Si vous déposez vos questions sur le serveur UNISCIEL faq2sciences, vous pouvez utiliser le champ "mots clés" de moodle, en faisant commencer votre mot clé par (sans guillemet) :
	<ul>
		<li>"theme:" suivi du nom de code Scenari du <a href="https://framagit.org/stephanep/moodle2scenari/blob/master/quizConfig.js" target="_blank">thème</a>, ou de l'intitulé du thème (les tags moodle ne peuvent pas contenir de "," donc mettez ";" à la place)</li>
		<li>"themelycee:" même chose pour les thèmes lycées, ceux-ci ne doivent être utilisé que pour des besoins très spécifiques</li>
		<li>"complexite:" suivi d'un chiffre de 1 à 4 qui décrit le niveau de complexité de bloom</li>
		<li>"niveau:" suivi d'un élément dans la liste : L0, L1, L2, L3, M1, M2. Il est possible d'avoir plusieurs fois le mot clé "niveau:..." si la question est valide pour plusieurs niveaux, mais pas plusieurs niveaux au sein d'un même mot clé</li>
		<li>"origine:" suivi d'une phrase avec le nom de l'auteur et le nom de domaine de l'établissement de l'auteur</li>
	</ul>
</p>
<h2>Procédure</h2>
<p>
<ul>
	<li>
		Exportez un fichier au format Moodle XML depuis une banque de question Moodle
	</li>
	<li>
		Chargez votre fichier .xml dans l'emplacement ci-dessous.<br>
		<form action="?" method="post" enctype="multipart/form-data">
			<label for="file">Fichier Moodle XML :</label>
			<input type="hidden" name="MAX_FILE_SIZE" value="262144000"/>
			<input type="file" id="file" name="file"/>
			<input type="submit" value="Envoyer" name="submit" id="submit"/>
		</form>
	</li>
	<li>
		Dans Scenari, dans l'arbre des items (zone de gauche), faites clic droit -&gt; importer une archive, et choisissez le fichier .scar récupéré depuis ce site. Attention, si vous avez dans votre atelier un répertoire nommé "import-exercices-moodle", ce répertoire peut être remplacé lors du chargement de l'archive.
	</li>
</ul>
</p>
<p>
	Attention : la sécurité de cette première version du service n'est pas adapté à la conversion de documents confidentiels.
	Vous pouvez installer les scripts de conversion sur votre propre ordinateur (<a href="https://framagit.org/stephanep/moodle2scenari">lien vers les sources</a>)
</p>
<p>
	En cas d'erreur, vous pouvez <a href="mailto:stephane.poinsart@utc.fr">contacter l'administrateur de ce service de conversion.</a>
</p>


<?php
require_once('footer.inc.php');
?>
